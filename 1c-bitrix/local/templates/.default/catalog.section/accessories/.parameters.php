<?
/**
 * @author �������
 * @package pulsal.ru
 * @date 28.07.2015
 *
 * @autocomplete add_tmpl_param<TAB> - Adding the language phrases;
 *
 * ATTENTION !!! NOT ALLOWED:
 *  - any modifications of $arComponentParameters
 *  - using keys "GROUPS" or "PARAMETERS" in $arTemplateParameters (only array list of params with their options)
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true){ 
	die();
}
 
$arTemplateParameters = array(
	// add_tmpl_param<tab>
    'ADDITIONAL_SORT' => array(
        'PARENT' => 'ADDITIONAL_SETTINGS',
        'NAME' => GetMessage('ADDITIONAL_SORT'), // �������� ��������� �� ������� �����
        'TYPE' => 'STRING', // ��� �������� ����������, � ������� ����� ��������������� ��������
        'REFRESH' => 'N', // ����������� ��������� ��� ��� ����� ������ (N/Y)
        'MULTIPLE' => 'Y', // ���������/������������� �������� (N/Y)
        'VALUES' => '', // ������ �������� ��� ������ (TYPE = LIST)
        'ADDITIONAL_VALUES' => 'Y', // ���������� ���� ��� ��������, �������� ������� (Y/N)
        'SIZE' => '', // ������ ���� � ��������
        'DEFAULT' => 'Y', // �������� �� ���������
        'COLS' => '', // ����� ����� ��� ������ (���� ����� �� ���������� ������) ��� ������ ���� ��� ���� ���� STRING
    ),
);