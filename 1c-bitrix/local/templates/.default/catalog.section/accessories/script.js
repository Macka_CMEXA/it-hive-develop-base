/**
 * Created by Macka_CMEXA on 13.07.2015.
 */

$(document).ready(function () {
    $(function () {
        $(".jCarouselLite_accessory_conteiner").append("<a class=\"prev rotate_element_up\" href=\"#\"><b><!--&#9650;-->�</b></a><a class=\"next rotate_element_down\" href=\"#\"><b><!--&#9660;-->�</b></a>");
        $(".jCarouselLite_accessory").jCarouselLite({
            btnNext: ".jCarouselLite_accessory_conteiner .next",
            btnPrev: ".jCarouselLite_accessory_conteiner .prev",
            scroll: 4,
            auto: null,
            speed: 500,
            start: 0,

            visible:4,
            mouseWheel: false,
            vertical:true
        });

    });

    var $block = $('.accessory_conteiner .i_block');
    $block.css("height", "48px");

    /*�������� ���������� ����� posts � ��������� � ���� ������� ���� posts.moved ��� ������� ������� ��������*/
    $block.live("hover",function() {
        var offset = $(this).offset().top - $(this).parents('div').offset().top + 2;
        var $postMoved = $('.accessory_conteiner  .posts.moved');
        $postMoved.empty();
        $postMoved.append($(this).html()).css("top", offset).show().css("position", "absolute");
    });

    $(".accessory_conteiner .posts.moved").live({
        mouseleave: function () {
            $('.accessory_conteiner .posts.moved').empty().hide();
        }
    });

});