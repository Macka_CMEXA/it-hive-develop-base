<?
$MESS["CATALOG_BUY"] = "Buy";
$MESS["CATALOG_ADD"] = "Add to cart";
$MESS["CATALOG_COMPARE"] = "Compare";
$MESS["CATALOG_NOT_AVAILABLE"] = "(not available from stock)";
$MESS["CATALOG_QUANTITY"] = "Quantity";
$MESS["CATALOG_QUANTITY_FROM_TO"] = "from #FROM# to #TO#";
$MESS["CATALOG_QUANTITY_FROM"] = "#FROM# and more";
$MESS["CATALOG_QUANTITY_TO"] = "up to #TO#";
$MESS["CT_BCS_QUANTITY"] = "Quantity";
$MESS["CT_BCS_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["CATALOG_SORT_BY"] = "Sort by";
$MESS["CATALOG_SORT_BY_NAME"] = "name";
$MESS["CATALOG_SORT_BY_PRICE"] = "price";
$MESS["CATALOG_SORT_BY_NEW"] = "new";
$MESS["CATALOG_CATALOG_TYPE"] = "Catalog type";
$MESS["CATALOG_VIEW"] = "Show";
$MESS["CATALOG_OF"] = "of";
$MESS["CATALOG_OPTIONS"] = "options";
$MESS["CATALOG_SELECT"] = "Select";
$MESS["CATALOG_DOP_INFO"] = "For more information, please contact your sales managers by phone";
$MESS["CATALOG_CNT"] = "Show products per page";
?>