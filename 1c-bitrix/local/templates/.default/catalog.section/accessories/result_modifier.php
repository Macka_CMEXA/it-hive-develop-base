<?
/**
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// sort forward property
if(isset($arParams['ADDITIONAL_SORT']) && is_array($arParams['ADDITIONAL_SORT'])){
    usort($arResult['ITEMS'], function($a, $b) use ($arParams) {
        return (array_search($a['ID'], $arParams['ADDITIONAL_SORT']) < array_search($b['ID'], $arParams['ADDITIONAL_SORT'])) ? '-1' : '1';
    });
}

foreach ($arResult["ITEMS"] as $key => &$arElement) {
    $PIC_HEIGHT = $PIC_WIDTH = 47;
    $PICTURE = is_array($arElement["PREVIEW_PICTURE"]) ? $arElement["PREVIEW_PICTURE"] : $arElement["DETAIL_PICTURE"];
    if (is_array($PICTURE)) {
        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
        $file = CFile::ResizeImageGet($PICTURE, array('width' => $PIC_WIDTH, 'height' => $PIC_HEIGHT), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
        $arElement["PREVIEW_PICTURE"]["SRC"] = $file["src"];
        $arElement["PREVIEW_PICTURE"]["WIDTH"] = $file["width"];
        $arElement["PREVIEW_PICTURE"]["HEIGHT"] = $file["height"];
    }
}
unset($arElement);