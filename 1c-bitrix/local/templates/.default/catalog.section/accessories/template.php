<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * @var array $arResult
 * @var array $arParams
 */
if ($arResult["ITEMS"]): ?>
    <div class="slider_block i_block_container">
        <h2 class="blue"><?= GetMessage("BLOCK_TITLE_ACCESSORY") ?></h2>
        <div class="<? echo (count($arResult["ITEMS"]) > $arParams["LINE_ELEMENT_COUNT"]) ? 'jCarouselLite_accessory_conteiner' : '' ?> accessory_conteiner" >

            <div class="posts moved clearfix"></div>
            <div class=" catalog in_row_<?= $arParams["LINE_ELEMENT_COUNT"] ?> separators">
                <? if ($arParams["TITLE"]) { ?><h2 class="title"><?= $arParams["TITLE"] ?></h2><? } ?>
                <div <? if (count($arResult["ITEMS"]) > $arParams["LINE_ELEMENT_COUNT"]): ?>class="jCarouselLite_accessory"<? endif; ?> >
                    <ul class="i_block_container">
                        <?
                        //$arResult["ITEMS"] = array_map("unserialize", array_unique( array_map("serialize", $arResult["ITEMS"]) ));

                        foreach ($arResult["ITEMS"] as $cell => $arElement):?>


                            <?

                            if (!empty($arElement["PROPERTIES"]["CML2_ARTICLE"]["VALUE"])) {

                                $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                <li class="i_block" itemscope itemtype="http://schema.org/Product"
                                    data-itemid="<?= $arElement["ID"] ?>"
                                    id="<?= $this->GetEditAreaId($arElement['ID']); ?>">
                                    <?
                                    //echo "<pre>"; print_r($arElement); echo "</pre>";
                                    ?>
                                    <div class="img left_block">
                                        <? if ($arElement["DISCOUNT_DIFF_PERCENT"]) {
                                            ?>
                                            <span
                                                class="discount_label">-<? echo $arElement["DISCOUNT_DIFF_PERCENT"] ?>
                                                %</span>
                                        <? } ?>
                                        <? if ($arElement["PREVIEW_PICTURE"]["SRC"]) {
                                            ?>
                                            <a href="<?= $arElement["DETAIL_PAGE_URL"] ?>"><img
                                                    src="<?= $arElement["PREVIEW_PICTURE"]["SRC"] ?>"
                                                    alt="<?= $arElement["NAME"] ?>" itemprop="image"></a>
                                        <? } ?>
                                    </div>
                                    <div class="rght_block">

                                        <div class="buy_block">
                                            <div class="price-catalog">
                                                <?

                                                if (count($arElement["PRICES"]) > 0) {
                                                    foreach ($arElement["PRICES"] as $code => $arPrice) {
                                                        if ($arPrice["VALUE"] >= 1000) {
                                                            $arPrice["VALUE"] = number_format($arPrice["VALUE"], 2, ',', '.');
                                                            $cent = substr($arPrice["VALUE"], -2);
                                                            $hundred = substr($arPrice["VALUE"], strlen($arPrice["VALUE"] % 1000) + 1, -1 - strlen($cent));
                                                            $thousand = substr($arPrice["VALUE"], 0, -strlen($hundred) - strlen($cent) - 2);
                                                        }
                                                        else {
                                                            $arPrice["VALUE"] = number_format($arPrice["VALUE"], 2, ',', '.');
                                                            $cent = substr($arPrice["VALUE"], -2);
                                                            $hundred = substr($arPrice["VALUE"], 0, -1 - strlen($cent));
                                                            $thousand = "";
                                                        }

                                                        if ($arPrice["DISCOUNT_VALUE"] >= 1000) {
                                                            $arPrice["DISCOUNT_VALUE"] = number_format($arPrice["DISCOUNT_VALUE"], 2, ',', '.');
                                                            $cent2 = substr($arPrice["DISCOUNT_VALUE"], -2);
                                                            $hundred2 = substr($arPrice["DISCOUNT_VALUE"], strlen($arPrice["DISCOUNT_VALUE"] % 1000) + 1, -1 - strlen($cent2));
                                                            $thousand2 = substr($arPrice["DISCOUNT_VALUE"], 0, -strlen($hundred2) - strlen($cent2) - 2);
                                                        }
                                                        else {
                                                            $arPrice["DISCOUNT_VALUE"] = number_format($arPrice["DISCOUNT_VALUE"], 2, ',', '.');
                                                            $cent2 = substr($arPrice["DISCOUNT_VALUE"], -2);
                                                            $hundred2 = substr($arPrice["DISCOUNT_VALUE"], 0, -1 - strlen($cent2));
                                                            $thousand2 = "";
                                                        }
                                                        if ($arPrice["CAN_ACCESS"]) {
                                                            if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>

                                                                <span
                                                                    class="price-catalog-old"><?= $thousand ?><?= $hundred ?>
                                                                    ���</span>
                                                                <span class="price-catalog-new"
                                                                      itemprop="price"><strong><?= $thousand2 ?></strong><?= $hundred2 ?><span><? if ($cent2 > 0) {
                                                                            echo "," . $cent2;
                                                                        } ?></span> ���</span>

                                                            <? else:?>
                                                                <span
                                                                    itemprop="price"><strong><?= $thousand ?></strong> <?= $hundred ?>
                                                                    ���</span>
                                                            <? endif; ?>
                                                        <? } ?>
                                                    <? } ?>
                                                <? } ?>
                                                <? if ($arElement['CATALOG_QUANTITY'] > 0) { ?>
                                                    <div class="catalog_quantity available_quantity">� �������</div>
                                                <?
                                                }
                                                else { ?>
                                                    <div class="catalog_quantity not_available_quantity bTooltip">
                                                        <? if (is_numeric($arElement['PROPERTIES']['SUPL_TIME']['VALUE'])) { ?>
                                                            ��
                                                            <? if ($arElement['PROPERTIES']['SUPL_TIME']['VALUE'] == "1") {
                                                                echo($arElement['PROPERTIES']['SUPL_TIME']['VALUE']); ?> ������
                                                            <? }
                                                            else {
                                                                echo($arElement['PROPERTIES']['SUPL_TIME']['VALUE']); ?> ������ <? } ?>
                                                        <? }
                                                        else { ?>
                                                            �� �������
                                                        <? } ?>
                                                        <div class="eTooltipCover">
                                                            <div class="eTooltip">
                                                                ����� �������� ����� �������� � ���������<br>��
                                                                ���.: +7 (812) 640-30-02<br>��� �� �����������
                                                                �����: <a href="mailto:info@pulsal.ru">info@pulsal.ru</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? } ?>
                                            </div>
                                        </div>
                                        <div class="props">
                                        <span class="element-props">
                                            <a target="_blank" href="<?= $arElement["DETAIL_PAGE_URL"] ?>"
                                               class="link" itemprop="url"><span
                                                    itemprop="name"><?= $arElement["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] ?></span></a>
                                            <br/>
                                            <span>
                                            <?
                                            if ($arElement["DISPLAY_PROPERTIES"]["PRODUSER"]["LINK_ELEMENT_VALUE"]) {
                                                foreach ($arElement["DISPLAY_PROPERTIES"]["PRODUSER"]["LINK_ELEMENT_VALUE"] as $VALUE) {
                                                    echo $VALUE["NAME"] . ", ";
                                                }
                                            }
                                            ?>
                                            <? if ($arElement["PROPERTIES"]["CML2_ARTICLE"]["VALUE"]) {
                                                echo $arElement['PROPERTIES']['CML2_ARTICLE']['VALUE'];
                                            } ?>
                                            </span>
                                        </span>
                                        </div>
                                        <div style="clear: both;"></div>
                                        <div class="posts clearfix" style="float:left; width: 100%;">
                                            <div class="can_buy_block">
                                                <? if ($arElement['CAN_BUY']): ?>
                                                <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                                                    <br/><? endif ?>
                                                <input style="text-align: center;" maxlength="3"
                                                       onkeyup="this.value = this.value.replace (/\D/, '')"
                                                       type="text" class="quantity"
                                                       name="quantity<?= $arElement['ID'] ?>"
                                                       id="quantity<?= $arElement['ID'] ?>" value="1" size="2" title="quantity">
                                                <a class="btn-buy" href="<? echo $arElement["ADD_URL"] ?>"
                                                   rel="nofollow"
                                                   onclick="return addToCart(this, 'catalog_list_image_<?= $arElement['ID'] ?>', 'list', '<?= GetMessage("CATALOG_IN_CART") ?>', '<?= $arElement["ID"] ?>');"
                                                   id="catalog_add2cart_link_<?= $arElement['ID'] ?>">
                                                    ������
                                                </a>
                                            </div>
                                            <? elseif (count($arResult["PRICES"]) > 0):?>
                                                <span
                                                    class="catalog-item-not-available"><?= GetMessage('CATALOG_NOT_AVAILABLE') ?></span>
                                            <? endif;
                                            ?>


                                            <div class="item_posts_block">
                                                <? if ($arElement["DISPLAY_PROPERTIES"]["FRAME_GANG"]) {
                                                    ?>
                                                    <?
                                                    foreach ($arElement["POSTS"] as $POSTS) {
                                                        echo '<a target="_blank" href="' . $POSTS["DETAIL_PAGE_URL"] . '" class="item_posts">' . "�����" . " " . $POSTS["NAME"] . '</a>';
                                                    }
                                                    ?>

                                                <? } ?>
                                            </div>
                                        </div>

                                    </div>

                                </li>

                            <? } endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>